FROM python:3.7 AS builder

WORKDIR /tmp/repo
COPY setup.py requirements.txt /tmp/repo/
COPY nnfwtbn /tmp/repo/nnfwtbn
RUN pip install -r requirements.txt
RUN pip install .

FROM python:3.7
COPY --from=builder /usr /usr
