"""
This script is used to install the package and all its dependencies. Run

    python setup.py install 

to install the package.
"""

from setuptools import setup


setup(name='nnfwtbn',
      version='0.0.0',  # Also change in module
      packages=["nnfwtbn", "nnfwtbn.tests"],
      install_requires=["cloudpickle",
                        "h5py",
                        "numpy",
                        "matplotlib",
                        "seaborn",
                        "tables",
                        "pandas",
                        "pylorentz",
                        "atlasify>=0.2.0",
                        "tensorflow",
                        "keras",
                        "dask",
                        "dask[complete]",
                        "uhepp",
                        "lxml"],
      test_suite='nnfwtbn.tests',
      description='Experimental neural network framework to be named.',
      url="https://gitlab.cern.ch/fsauerbu/nnfwtbn",
      author="Frank Sauerburger",
      author_email="f.sauerburger@cern.ch",
      license="MIT",
      classifiers=["Intended Audience :: Science/Research",
                   "License :: OSI Approved :: MIT License",
                   "Operating System :: OS Independent",
                   "Programming Language :: Python :: 3.5",
                   "Programming Language :: Python :: 3.6",
                   "Programming Language :: Python :: 3.7",
                   "Topic :: Scientific/Engineering :: Physics"])
