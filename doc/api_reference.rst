*************
API reference
*************

.. autoclass:: nnfwtbn.Cut
   :members:
   :special-members:

.. autoclass:: nnfwtbn.Process
   :members:
   :special-members:


.. autoclass:: nnfwtbn.Variable
   :members:
   :special-members:

.. autoclass:: nnfwtbn.variable.BlindingStrategy
   :members:
   :special-members:

.. autoclass:: nnfwtbn.RangeBlindingStrategy
   :members:
   :special-members:

.. autoclass:: nnfwtbn.CrossValidator
   :members:
   :special-members:

.. autoclass:: nnfwtbn.ClassicalCV
   :members:
   :special-members:

.. autoclass:: nnfwtbn.MixedCV
   :members:
   :special-members:

.. autoclass:: nnfwtbn.Normalizer
   :members:
   :special-members:

.. autoclass:: nnfwtbn.EstimatorNormalizer
   :members:
   :special-members:

.. autoclass:: nnfwtbn.HepNet
   :members:
   :special-members:

.. automodule:: nnfwtbn.plot
	:members:
