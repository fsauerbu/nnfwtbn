Neural Network Framework To Be Named
====================================

Pure Python framework to train neural networks for high energy physics analysis.

Examples
--------
 * `ToyData <https://nnfwtbn.web.cern.ch/examples/ToyData.html>`_
 * `Histogram <https://nnfwtbn.web.cern.ch/examples/Histogram.html>`_
 * `HistogramFactory <https://nnfwtbn.web.cern.ch/examples/HistogramFactory.html>`_
 * `ConfusionMatrix <https://nnfwtbn.web.cern.ch/examples/ConfusionMatrix.html>`_
 * `CorrelationMatrix <https://nnfwtbn.web.cern.ch/examples/Correlation.html>`_
 * `ROC <https://nnfwtbn.web.cern.ch/examples/ROC.html>`_
 * `Classification <https://nnfwtbn.web.cern.ch/examples/Classification.html>`_
 * `TMVA BDT interface <https://nnfwtbn.web.cern.ch/examples/TmvaBdt.html>`_
 * `Blinding <https://nnfwtbn.web.cern.ch/examples/Blinding.html>`_

Links
-----
 * `Documentation <https://nnfwtbn.web.cern.ch/>`_
